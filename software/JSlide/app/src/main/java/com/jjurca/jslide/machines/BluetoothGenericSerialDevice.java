package com.jjurca.jslide.machines;

import android.bluetooth.BluetoothDevice;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.harrysoft.androidbluetoothserial.BluetoothManager;
import com.harrysoft.androidbluetoothserial.BluetoothSerialDevice;
import com.harrysoft.androidbluetoothserial.SimpleBluetoothDeviceInterface;
import com.jjurca.jslide.ui.machine.ConnectionListener;
import com.jjurca.jslide.exceptions.BluetoothException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class BluetoothGenericSerialDevice {
    private String bluetoothName = "";

    private ArrayList<ConnectionListener> mListener = new ArrayList<>();

    private SimpleBluetoothDeviceInterface deviceInterface;
    private BluetoothManager bluetoothManager;

    public MutableLiveData<String> deviceLog = new MutableLiveData<>("");

    public BluetoothGenericSerialDevice(){
         bluetoothManager = BluetoothManager.getInstance();
    }

    public void connect() throws BluetoothException {
        for (ConnectionListener x: mListener){
            x.onConnecting();
        }
        String mac = getMac(bluetoothName);
        bluetoothManager.openSerialDevice(mac)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onConnected, this::onError);
    }

    private void onConnected(com.harrysoft.androidbluetoothserial.BluetoothSerialDevice connectedDevice) {
        deviceInterface = connectedDevice.toSimpleDeviceInterface();

        // Listen to bluetooth events
        deviceInterface.setListeners(this::onMessageReceived, this::onMessageSent, this::onError);

        for (ConnectionListener x: mListener){
            x.onConnected();
        }
    }

    public void disconnect(){
        try {
            String mac = getMac(bluetoothName);
            bluetoothManager.closeDevice(mac);
            for (ConnectionListener x: mListener){
                x.onDisconnected();
            }
        } catch (BluetoothException e) {
            e.printStackTrace();
        }
    }

    private void onMessageSent(String message) {
        String date = new SimpleDateFormat("[HH:mm:ss]").format(Calendar.getInstance().getTime());
        String spacer = "\n";
        if (message.endsWith("\n")){
            spacer = "";
        }
        deviceLog.setValue( date + " --> " + message + spacer + deviceLog.getValue());

        Log.v("BT_Sent", message);
        for (ConnectionListener x: mListener){
            x.onSent(message);
        }
    }

    private void onMessageReceived(String message) {
        String date = new SimpleDateFormat("[HH:mm:ss]").format(Calendar.getInstance().getTime());
        String spacer = "\n";
        if (message.endsWith("\n")){
            spacer = "";
        }
        deviceLog.setValue( date + " <-- " + message + spacer + deviceLog.getValue());

        Log.v("BT_received",message);
        for (ConnectionListener x: mListener){
            x.onReceived(message);
        }
    }

    private void onError(Throwable error) {
        Log.v("BT_Error", error.toString());
        for (ConnectionListener x: mListener){
            x.onError();
        }
    }

    public boolean send(String s) {
        try {
            deviceInterface.sendMessage(s);
        } catch (NullPointerException e) {
            return false;
        }
        return true;
    }

    public String getMac(String name) throws BluetoothException {
        // Setup our BluetoothManager
        BluetoothManager bluetoothManager = BluetoothManager.getInstance();
        if (bluetoothManager != null) {
            Collection<android.bluetooth.BluetoothDevice> pairedDevices = bluetoothManager.getPairedDevicesList();
            for (android.bluetooth.BluetoothDevice device : pairedDevices) {
                if (device.getName().equals(name)){
                    return device.getAddress();
                }
            }
        }
        throw new BluetoothException("No mac address were found. First pair the device.");
    }

    public String getBluetoothName(){
        return  bluetoothName;
    }

    public boolean isAvailable(){
        BluetoothManager bluetoothManager = BluetoothManager.getInstance();
        if (bluetoothManager == null){
            return false;
        } else {
            return true;
        }
    }

    public void setConnectionListener(ConnectionListener mListener)
    {
        this.mListener.add(mListener);
    }

}
