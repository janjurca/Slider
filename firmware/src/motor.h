#ifndef MOTOR_H
#define MOTOR_H 1

#include <Arduino.h>
#include <TimerOne.h>
#include "bluetooth.h"
#include "commands.h"

#define MAXSTEP 17200LL
extern Bluetooth bluetooth;

class Motor {
public:
  enum direction { In = 0 , Out = 1 };
  enum mode { Timed = 0 , RPMSpeed = 1 };
  enum state { Started = 0 , Stopped = 1 , Paused = 2 };
  enum stepSizeEnum { Full = 1 ,Half = 2 ,Quarter = 4,Eighth = 8,Sixteenth = 16, Thirtieth = 32};


private:
    enum direction direction = In;
    enum mode mode = Timed;
    enum state state = Stopped;
    int8_t pin_step;
    int8_t pin_direction;
    int8_t ms1;
    int8_t ms2;
    int8_t ms3;
    int8_t endstop;
    int8_t pin_enable;
    int stepSize = Full;
    double stepcount = MAXSTEP;
    bool homed = false;
    long stepDelay = -1;
    long long totalTimeSpeed = 600;
    long moveToDestinationStep = -1;

public:
    void setStepSize(stepSizeEnum size);
    void setStepDelay(long delay);
    Motor(int8_t pin_step,int8_t pin_direction,int8_t ms1,int8_t ms2,int8_t ms3,int8_t pin_enable, int8_t endstop) :
        pin_step(pin_step),
        pin_direction(pin_direction),
        ms1(ms1),
        ms2(ms2),
        ms3(ms3),
        endstop(endstop),
        pin_enable(pin_enable){
        }


    int getStepSize(){
      return stepSize;
    }

    bool isSensorPushed(){
      return (digitalRead(endstop) != 0)? true: false;
    }

    /**
     * Initialize pins to output
     */
    void begin();

    /**
     * go to home position with motor, it should be run every time you want to use motor
     */
    void home();

    /**
     * Get current position, so you have to compute current state from the various lenght steps
     * @return [description]
     */
    double getPosition();

    /**
     * Get maximum steps count
     * @return [description]
     */
    long getLenght();


    /**
     * Set rpm of motor
     * @param rpm rpm number
     */
    void setSpeedRPM(int rpm);

    /**
     * Set time that takes to go throught
     * @param s full time
     */
    void setSpeedTime(long long s);

    /**
     * Enables motor driver
     * @param e if true then enable
     */
    void enable(bool e);
    bool isEnabled();

    void init();
    void start(bool cleanDest = true);
    void pause();
    void stop();
    void step();

    long getStepDelay(){
        return stepDelay;
    }

    void toggleDirection(){
        if (direction == In) {
            setDirection(Out);
        } else{
            setDirection(In);
        }
    }




    void setDirection(enum direction dir){
        direction = dir;
        digitalWrite(pin_direction, dir);
        bluetooth.send_command(Commands::getDirection, direction);
    }

    int getDirection(){
        return digitalRead(pin_direction);
    }


    int getState(){
        return state;
    }
    void setState(enum state v){
      state = v;
      bluetooth.send_command(Commands::getState, v);
    }


    int getMode(){
      return mode;
    };
    void setMode(enum mode m){
      mode = m;
      bluetooth.send_command(Commands::getMode, m);
    }








    long getTotalTimeSpeed(){
        return totalTimeSpeed;
    }
    void setTotalTimeSpeed(long long v){
        totalTimeSpeed = v;
        bluetooth.send_command(Commands::getTotalTimeSpeed,(long) v);
    }



    bool isHomed(){
      return homed;
    }
    void setHomed(bool v){
      homed = v;
      bluetooth.send_command(Commands::isHomed, v);
    }

    void moveToPoisiton(long stepcount){
      if (!isHomed()) {
        bluetooth.send_command(Commands::isHomed, isHomed());
        bluetooth.send_command(Commands::moveToPosition, "Cannot move to position. First home device.");
      } else {
        pause();
        moveToDestinationStep = stepcount;
        if (stepcount > this->stepcount) {
          setDirection(Out);
        } else {
          setDirection(In);
        }
        setSpeedTime(30);
        start(false);
      }


    }
};


#endif
