#include <Arduino.h>
#include <motor.h>
#include "battery.h"
#include "bluetooth.h"
#include "defines.h"
#include "commands.h"

Motor motor(MOTOR_STEP_PIN, MOTOR_DIRECTION_PIN, MOTOR_MS1_PIN, MOTOR_MS2_PIN, MOTOR_MS3_PIN, MOTOR_ENABLE_PIN, MOTOR_ENDSTOP);
Bluetooth bluetooth(BT_RX, BT_TX);
Battery battery(A6, 22000.0, 47000.0);
void executeCommand();

void setup()
{
    Serial.begin(115200);

    bluetooth.begin(KEY_PIN);
    motor.begin();
    motor.enable(false);
    motor.setSpeedRPM(10);
    motor.setDirection(Motor::Out);


}

typedef enum { S, Command, ReadingCommand, Value, ReadingValue } States;
int state = S;
String buffer = "";
int actualCommand = 0;
int actualValue = 0;
bool valueSet = false;

String input_buffer = "";
unsigned long last_recevied = 0;

void loop()
{
    while (bluetooth.available()) {
      char c = bluetooth.read();
      input_buffer += c;
      last_recevied = millis();
    }

    if(!bluetooth.available() && input_buffer.length() > 0 && millis() - last_recevied > 50) {
        for (size_t i = 0; i < input_buffer.length(); i++) {
            char c = input_buffer.charAt(i);
            if (isprint(c)) {
#ifdef DEBUG
                Serial.write(c);
#endif
                switch (state) {
                    case S:{
                      buffer = "";
                      valueSet = false;
                      if (c == 'C') {
                        state = Command;
                      }
                      break;
                    }
                    case Command:{
                      if (c >='0' && c <= '9') {
                        buffer += c;
                        state = ReadingCommand;
                      } else {
                        buffer = "";
                        state = S;
                      }
                      break;
                    }
                    case ReadingCommand:{
                      if (c >='0' && c <= '9') {
                        buffer += c;
                      } else if (c == 'V') {
                        actualCommand = buffer.toInt();
                        buffer = "";
                        state = Value;
                      } else if (c == ';') {
                        actualCommand = buffer.toInt();
                        executeCommand();
                        buffer = "";
                        state = S;
                      } else {
                        buffer = "";
                        state = S;
                      }
                      break;
                    }
                    case Value:{
                      if (c >='0' && c <= '9') {
                        buffer += c;
                        state = ReadingValue;
                      } else {
                        buffer = "";
                        state = S;
                      }
                      break;
                    }
                    case ReadingValue:{
                      if (c >='0' && c <= '9') {
                        buffer += c;
                      } else if (c == ';') {
                        actualValue = buffer.toInt();
                        valueSet = true;
                        executeCommand();
                        buffer = "";
                        state = S;
                      } else {
                        buffer = "";
                        state = S;
                      }
                      break;
                    }
                }
            }
        }
        input_buffer = "";
    }
}

void executeCommand(){
    switch (actualCommand) {
        case Commands::ping:{
            bluetooth.send_command(actualCommand);
            break;
        }
        case Commands::setDirection:{
            if (valueSet) {
                motor.setDirection((actualValue)? Motor::Out : Motor::In  );
                bluetooth.send_command(actualCommand, motor.getDirection());
            } else {
                bluetooth.send_command(actualCommand, String("Direction value unset"));
            }
            break;
        }
        case Commands::setTime:{
            if (valueSet) {
                motor.setSpeedTime(actualValue);
                bluetooth.send_command(actualCommand, motor.getTotalTimeSpeed());
            } else {
                bluetooth.send_command(actualCommand, String("Time value unset"));
            }
            break;
        }
        case Commands::setRpmspeed:{
            if (valueSet) {
                motor.setSpeedRPM(actualValue);
                bluetooth.send_command(actualCommand, actualValue);
            } else {
                bluetooth.send_command(actualCommand, String("RPM value unset"));
            }
            break;
        }
        case Commands::start:{
            motor.start();
            break;
        }
        case Commands::stop:{
            motor.stop();
            bluetooth.send_command(actualCommand);
            break;
        }
        case Commands::home:{
            motor.home();
            bluetooth.send_command(actualCommand);
            break;
        }
        case Commands::getPosition: {
            bluetooth.send_command(actualCommand, motor.getPosition());
            break;
        }
        case Commands::getLength: {
            bluetooth.send_command(actualCommand,motor.getLenght());
            break;
        }
        case Commands::enable: {
            if (valueSet) {
                bool en = (actualValue)? true : false;
                motor.enable(en);
            } else {
                bluetooth.send_command(actualCommand, "Enable value unset");
            }
            break;
        }
        case Commands::pause:{
            motor.pause();
            bluetooth.send_command(actualCommand, 1);
            break;
        }
        case Commands::getStepSize:{
            bluetooth.send_command(actualCommand, motor.getStepSize());
            break;
        }
        case Commands::getVoltage:{
            bluetooth.send_command(actualCommand, battery.getVoltage());
            break;
        }
        case Commands::getState: {
            bluetooth.send_command(actualCommand, motor.getState());
            break;
        }
        case Commands::getDirection: {
            bluetooth.send_command(actualCommand, motor.getDirection());
            break;
        }
        case Commands::getStepDelay: {
            bluetooth.send_command(actualCommand, motor.getStepDelay());
            break;
        }
        case Commands::getMode: {
            bluetooth.send_command(actualCommand, motor.getMode());
            break;
        }
        case Commands::isHomed: {
            bluetooth.send_command(actualCommand, motor.isHomed());
            break;
        }
        case Commands::isEnabled: {
            bluetooth.send_command(actualCommand, motor.isEnabled());
            break;
        }
        case Commands::isSensorPushed: {
            bluetooth.send_command(actualCommand, motor.isSensorPushed());
            break;
        }
        case Commands::getTotalTimeSpeed: {
            bluetooth.send_command(actualCommand, motor.getTotalTimeSpeed());
            break;
        }
        case Commands::moveToPosition: {
            if (valueSet) {
                motor.moveToPoisiton(actualValue);
            } else {
                bluetooth.send_command(actualCommand, "Steps value unset");
            }
            break;
        }
    }
}
