#ifndef COMMANDS_H
#define COMMANDS_H 1

class Commands {

  public:
    Commands();

    typedef enum {
      ping = 0,
      setDirection = 1,
      setTime = 2,
      setRpmspeed = 3,
      start = 4,
      stop = 5,
      home = 6,
      getPosition = 7,
      getLength = 8,
      enable = 9,
      pause = 10,
      getStepSize = 11,
      getVoltage = 12,

      getState = 13,
      getDirection = 14,
      getStepDelay = 15,
      getMode = 16,
      isHomed = 17,
      isEnabled = 18,
      isSensorPushed = 19,
      getTotalTimeSpeed = 20,
      moveToPosition = 21,

    } Commands_t;
};



#endif
