#include "motor.h"

extern Motor motor;

void Motor::begin(){
    pinMode(pin_step, OUTPUT);
    pinMode(pin_direction, OUTPUT);
    pinMode(ms1, OUTPUT);
    pinMode(ms2, OUTPUT);
    pinMode(ms3, OUTPUT);
    pinMode(endstop, INPUT_PULLUP);
    pinMode(pin_enable, OUTPUT);

    setStepSize(Full);
    enable(false);
    setSpeedTime(totalTimeSpeed);
    setDirection(direction);
}

void Motor::enable(bool e){
    bluetooth.send_command(Commands::isEnabled, e);
    if (e) {
        digitalWrite(pin_enable, LOW);
    } else {
        stepcount = MAXSTEP;
        setHomed(false);
        setState(Stopped);
        digitalWrite(pin_enable, HIGH);
    }
}

bool Motor::isEnabled(){
    if (digitalRead(pin_enable) == LOW) {
      return true;
    }
    return false;
}

void Motor::setStepDelay(long delay){
    Timer1.initialize(delay);
    stepDelay = delay;
}

void Motor::setSpeedRPM(int rpm){
    if (rpm <= 0) {
        stop();
    }
    if (rpm > 750) {
        setStepSize(Half);
    } else if (rpm > 450) {
        setStepSize(Quarter);
    } else if (rpm > 160) {
        setStepSize(Eighth);
    } else if (rpm > 80) {
        setStepSize(Sixteenth);
    } else {
        setStepSize(Thirtieth);
    }
    setStepDelay((long)(60000000UL/((long long) rpm * (long long)stepSize * 200UL)));
    setMode(RPMSpeed);
}

void Motor::setSpeedTime(long long s){
    if (s > 30) {
        setStepSize(Thirtieth);
    } else if (s > 15) {
        setStepSize(Sixteenth);
    } else {
        setStepSize(Eighth);
    }
    setStepDelay((long)((s*1000000LL)/(MAXSTEP*stepSize)));
    setMode(Timed);
    setTotalTimeSpeed(s);
}

void Motor::start(bool cleanDest){
    if (cleanDest) {
      moveToDestinationStep = -1;
    }
    enable(true);
    bluetooth.send_command(Commands::start);
    Timer1.attachInterrupt([=]{motor.step();});
    setState(Started);
}

void Motor::stop(){
    enable(false);
    Timer1.detachInterrupt();
    setState(Stopped);
}

void Motor::pause(){
    Timer1.detachInterrupt();
    setState(Paused);
}

void Motor::step(){
    //---------------------- MOVE TO DESTINATION BLOCK -----------------
    if (moveToDestinationStep > 0) {
        if ( direction == Motor::In ) {
            if (stepcount < moveToDestinationStep) {
              pause();
              moveToDestinationStep = -1;
              return;
            }
        } else if (direction == Motor::Out) {
            if (stepcount > moveToDestinationStep) {
                pause();
                moveToDestinationStep = -1;
                return;
            }
        }
    }
    //------------------------------------------------------------------


    if (digitalRead(endstop) != 0 && direction == Motor::In) {
        stepcount = 0;
        setHomed(true);
        pause();
        return;
    }
    if (stepcount > MAXSTEP && direction == Motor::Out) {
        pause();
        return;
    }

    if ( direction == Motor::In ) {
        stepcount -= 1./stepSize;
    } else if ( direction == Motor::Out ) {
        stepcount += 1./stepSize;
    }
    static bool powered = false;
    if (powered) {
        digitalWrite(pin_step, HIGH);
        powered = false;
    } else {
        digitalWrite(pin_step, LOW);
        powered = true;
    }

}

void Motor::setStepSize(stepSizeEnum size){
    stepSize = size;
    switch (size) {
        case Full:{
            digitalWrite(ms1, LOW);
            digitalWrite(ms2, LOW);
            digitalWrite(ms3, LOW);
            break;
        }
        case Half:{
            digitalWrite(ms1, HIGH);
            digitalWrite(ms2, LOW);
            digitalWrite(ms3, LOW);
            break;
        }
        case Quarter:{
            digitalWrite(ms1, LOW);
            digitalWrite(ms2, HIGH);
            digitalWrite(ms3, LOW);
            break;
        }
        case Eighth:{
            digitalWrite(ms1, HIGH);
            digitalWrite(ms2, HIGH);
            digitalWrite(ms3, LOW);
            break;
        }
        case Sixteenth:{
            digitalWrite(ms1, LOW);
            digitalWrite(ms2, LOW);
            digitalWrite(ms3, HIGH);
            break;
        }
        case Thirtieth:{
            digitalWrite(ms1, HIGH);
            digitalWrite(ms2, HIGH);
            digitalWrite(ms3, HIGH);
            break;
        }

    }

}

void Motor::home(){
    setDirection(Motor::In);
    setSpeedTime(40);
    start();
}

long Motor::getLenght(){
    return MAXSTEP;
}

double Motor::getPosition(){
    return stepcount;
}
