Slider
========
This is project of photographic slider that is based on arduino and can be controlled by phone through bluetooth.
Project consist of three parts. First is firmware code that runs on arduino. Second is hw board schematic and third is android app.

## Firmware

The firmware is written for arduino with help of PlatformIO.

## Hadware

Main part of project is HW part which consist of board that is used to controll nema14 motor using arduino, bluetooth module and stepper motor driver.
The other part is printable components of slider which is available on OnShape (https://cad.onshape.com/documents/7be35bc10c9bd0138f67a252/w/fe789a6c66bab80a07b0bd25/e/ea846d03661e99fd00c2e376).


## Software

Software is android app that anyone, who want to build this slider successfuly, should be able to build it like a charm :-).

## Contact

If anyone would want some help with that you can contact me through repository messages...
