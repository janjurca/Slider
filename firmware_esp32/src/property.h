#ifndef PROPERTY_H
#define PROPERTY_H value


class Property {
private:
  union Value {
      int INTEGER;
      const char* STRING;
      double DOUBLE;
      bool BOOL;
      const Property * CHILD;
  };

  Value m_value;

  enum Type { INT_T, DOUBLE_T, STRING_T, PROPERTY_T, BOOL_T};

  enum Type m_type;
  typedef void (*callback_function)(void);
  callback_function onchange = nullptr;
  const char * m_name = nullptr;
  int m_N = 0;


public:
  Property (const char* name, int v, callback_function func = nullptr){
    m_value.INTEGER = v;
    m_type = INT_T;
    onchange = func;
    m_name = name;
  }
  Property (const char* name, const char* v, callback_function func = nullptr){
    m_value.STRING = v;
    m_type = STRING_T;
    onchange = func;
    m_name = name;
  }
  Property (const char* name, double v, callback_function func = nullptr){
    m_value.DOUBLE = v;
    m_type = DOUBLE_T;
    onchange = func;
    m_name = name;
  }
  Property (const char* name, bool v, callback_function func = nullptr){
    m_value.BOOL = v;
    m_type = BOOL_T;
    onchange = func;
    m_name = name;
  }
  Property (const char* name, Property* v, int N=1, callback_function func = nullptr){
    m_value.CHILD = v;
    m_type = PROPERTY_T;
    onchange = func;
    m_name = name;
    m_N = N;
  }

  int Int(){
    return m_value.INTEGER;
  }
  const char* String(){
    return m_value.STRING;
  }
  double Double(){
    return m_value.DOUBLE;
  }
  double Bool(){
    return m_value.BOOL;
  }
  const Property* Child(){
    return m_value.CHILD;
  }
  int Child_count(){
    return m_N;
  }


  int Int(int v){
    m_value.INTEGER = v;
    if (onchange != nullptr) {
      onchange();
    }
    return m_value.INTEGER;
  }
  const char* String(const char* v){
    m_value.STRING = v;
    if (onchange != nullptr) {
      onchange();
    }
    return m_value.STRING;
  }
  double Double(double v){
    m_value.DOUBLE = v;
    if (onchange != nullptr) {
      onchange();
    }
    return m_value.DOUBLE;
  }
  double Bool(bool v){
    m_value.BOOL = v;
    if (onchange != nullptr) {
      onchange();
    }
    return m_value.DOUBLE;
  }
  const Property* Child(Property* v, int N){
    m_value.CHILD = v;
    m_N = N;
    if (onchange != nullptr) {
      onchange();
    }
    return m_value.CHILD;
  }

};

#endif
