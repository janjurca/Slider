#ifndef COMPONENT_H
#define COMPONENT_H value

#include "Arduino.h"
#include "property.h"
#include <Vector.h>

class Component {
protected:

  const char * m_name = nullptr;
  Vector<Property> m_properties;

public:
  Component (const char* name) : m_name(name){

  };


};

class LinearAxis : public Component{
public:
  LinearAxis(const char* name): Component(name){
    m_properties.push_back(Property("length", 17000));
    m_properties.push_back(Property("rpm", 0));
    m_properties.push_back(Property("position", -1));
    m_properties.push_back(Property("running", false));
  };

  

};



#endif
