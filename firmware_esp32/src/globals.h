#ifndef GLOBALS_H
#define GLOBALS_H value

#include <Arduino.h>
#include "BluetoothSerial.h"

#define BATTERY_PIN 6
#define MOTOR_STEP_PIN 6

#define MOTOR_MS1_PIN 4
#define MOTOR_MS2_PIN 4
#define MOTOR_MS3_PIN 3
#define MOTOR_ENABLE_PIN 6
#define MOTOR_DIRECTION_PIN 4
#define MOTOR_ENDSTOP_PIN 2

extern BluetoothSerial BTSerial;


#endif
