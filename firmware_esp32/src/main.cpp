/*
   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include <Arduino.h>
#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <driver/gpio.h>
#include "sdkconfig.h"
#include "component.h"


extern "C" {
  esp_err_t nvs_flash_init();
}

extern "C" void app_main(void)
{
  ESP_ERROR_CHECK( nvs_flash_init() );
  initArduino();
  //xTaskCreatePinnedToCore(bluetoothTask, "bluetoothTask", 2 * 1024, NULL, 5, NULL, 0);
}


#if CONFIG_AUTOSTART_ARDUINO
#warning Arduino is used
#endif
