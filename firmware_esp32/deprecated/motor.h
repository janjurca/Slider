#ifndef MOTOR_H
#define MOTOR_H value

#include "globals.h"

class Motor {
private:
  TaskHandle_t motor_task_handle = nullptr;

  enum Direction { In = 0 , Out = 1 };
  enum Mode { Simple = 0, Waypoints = 1 };
  enum State { Started = 0 , Stopped = 1 , Paused = 2 };
  enum stepSizeEnum { Full = 1 ,Half = 2 ,Quarter = 4,Eighth = 8,Sixteenth = 16, Thirtieth = 32};

  long m_step_delay = LONG_MAX;
  long m_speed = 600;
  double m_position = device.maxStep();
  double stepSize = Thirtieth;
  bool homed = false;

  enum Direction m_direction = In;
  enum State m_state = Stopped;
  enum Mode m_mode = Simple;

  void setState(enum State s){
    m_state = s;
  }



public:


  Motor (){};

  void begin(){
    pinMode(MOTOR_MS1_PIN, OUTPUT);
    pinMode(MOTOR_MS2_PIN, OUTPUT);
    pinMode(MOTOR_MS3_PIN, OUTPUT);
    pinMode(MOTOR_ENABLE_PIN, OUTPUT);
    pinMode(MOTOR_DIRECTION_PIN, OUTPUT);

    pinMode(MOTOR_ENDSTOP_PIN, INPUT_PULLUP);

    setStepSize(Thirtieth);
  }

  static void motor_runner_task(void * m){

      while (true) {
          long delay = motor.stepDelay();
          if (delay > 0 ) {
              motor.step();
              vTaskDelay(delay / portTICK_PERIOD_MS);
          } else {
              vTaskDelay(10 / portTICK_PERIOD_MS);
          }
      }
  }

  void step(){
      //---------------------- MOVE TO DESTINATION BLOCK -----------------
    //    if (moveToDestinationStep > 0) {
    //        if ( m->direction() == Motor::In ) {
    //            if (stepcount < moveToDestinationStep) {
    //              pause();
    //              moveToDestinationStep = -1;
    //              return;
    //            }
    //        } else if (direction == Motor::Out) {
    //            if (stepcount > moveToDestinationStep) {
    //                pause();
    //                moveToDestinationStep = -1;
    //                return;
    //            }
    //        }
    //    }
        //------------------------------------------------------------------


        if (digitalRead(MOTOR_ENDSTOP_PIN) != 0 && m_direction == Motor::In) {
            m_position = 0;
            setHomed(true);
            pause();
            return;
        }
        if (m_position > device.maxStep() && m_direction == Motor::Out) {
            pause();
            return;
        }

        if ( m_direction == Motor::In ) {
            m_position -= 1./stepSize;
        } else if ( m_direction == Motor::Out ) {
            m_position += 1./stepSize;
        }
        static bool powered = false;
        if (powered) {
            digitalWrite(MOTOR_STEP_PIN, HIGH);
            powered = false;
        } else {
            digitalWrite(MOTOR_STEP_PIN, LOW);
            powered = true;
        }
  }

  bool start(){
    if (motor_task_handle != nullptr) {
      xTaskCreatePinnedToCore(Motor::motor_runner_task, "motor_runner", 1024, nullptr, 5, &motor_task_handle, 1);
      setState(Started);
      return true;
    } else {
      setState(Stopped);
      return false;
    }
  }

  bool stop(){
    if (motor_task_handle != nullptr) {
      vTaskDelete(motor_task_handle);
      setState(Stopped);
      return true;
    } else {
      return false;
    }
  }

 void enable(bool e){
   digitalWrite(MOTOR_ENABLE_PIN, (e) ? HIGH: LOW);
 }

 bool isEnabled(){
   return (digitalRead(MOTOR_ENABLE_PIN) == HIGH) ? true : false;
 }

 void setSpeed(long duration){ //duration in seconds
   if (duration > 30) {
     setStepSize(Thirtieth);
   } else if (duration > 15) {
     setStepSize(Sixteenth);
   } else {
     setStepSize(Eighth);
   }
   m_speed = duration;
   m_step_delay = ((long)((duration*1000000LL)/(device.maxStep()*stepSize)));
 }


 void setStepSize(stepSizeEnum size){
     stepSize = size;
     switch (size) {
         case Full:{
             digitalWrite(MOTOR_MS1_PIN, LOW);
             digitalWrite(MOTOR_MS2_PIN, LOW);
             digitalWrite(MOTOR_MS3_PIN, LOW);
             break;
         }
         case Half:{
             digitalWrite(MOTOR_MS1_PIN, HIGH);
             digitalWrite(MOTOR_MS2_PIN, LOW);
             digitalWrite(MOTOR_MS3_PIN, LOW);
             break;
         }
         case Quarter:{
             digitalWrite(MOTOR_MS1_PIN, LOW);
             digitalWrite(MOTOR_MS2_PIN, HIGH);
             digitalWrite(MOTOR_MS3_PIN, LOW);
             break;
         }
         case Eighth:{
             digitalWrite(MOTOR_MS1_PIN, HIGH);
             digitalWrite(MOTOR_MS2_PIN, HIGH);
             digitalWrite(MOTOR_MS3_PIN, LOW);
             break;
         }
         case Sixteenth:{
             digitalWrite(MOTOR_MS1_PIN, LOW);
             digitalWrite(MOTOR_MS2_PIN, LOW);
             digitalWrite(MOTOR_MS3_PIN, HIGH);
             break;
         }
         case Thirtieth:{
             digitalWrite(MOTOR_MS1_PIN, HIGH);
             digitalWrite(MOTOR_MS2_PIN, HIGH);
             digitalWrite(MOTOR_MS3_PIN, HIGH);
             break;
         }
     }
 }

 void setHomed(bool v){
     homed = v;
 }

 bool isHomed(){
   return homed;
 }

 long position(){
     return m_position;
 }

 void setPosition(long pos){
     m_position = pos;
 }

 enum Direction direction(){
     return m_direction;
 }

 void setDirection(enum Direction d){
     m_direction = d;
 }

 void pause(){
     m_step_delay = -1;
 }

 long stepDelay(){
     return m_step_delay;
 }

 long getState(){
     return m_state;
 }

 long getSpeed(){
     return m_speed;
 }

 enum Mode getMode(){
   return m_mode;
 }

 bool isSensorPushed(){
   return (digitalRead(MOTOR_ENDSTOP_PIN) == HIGH) ? true: false;
 }


};
#endif
