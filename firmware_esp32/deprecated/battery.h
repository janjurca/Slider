#ifndef BATTERY_H
#define BATTERY_H 1

#include "Arduino.h"

class Battery {
private:
    typedef struct battery_value {
        int16_t value;
        unsigned long time;
    }bvalue_t;
    bvalue_t history_values[10];
    ///Analog pin number to measure voltage
    uint8_t pin;

    ///value of resistor that is connected to ground
    float ground_resistor;

    ///value that is connected to battery +
    float battery_resistor;

    ///Value of refference arduino ad convertor voltage
    float ref_volt;

    //Value that is used to compute real voltage from resistors
    float resistor_koef;

public:
    Battery (uint8_t pin, float ground_resistor,float battery_resistor, float ref_volt = 3.3):pin(pin),ground_resistor(ground_resistor),battery_resistor(battery_resistor), ref_volt(ref_volt){
        for (size_t i = 0; i < (sizeof(history_values)/sizeof(bvalue_t)); i++) {
            history_values[i].time = 0;
            history_values[i].value = 0;
        }
        resistor_koef = ground_resistor/(battery_resistor+ground_resistor);
        pinMode(pin, INPUT);
    };

    /**
     * Returns value of volts measured from Battery
     * @return float number of volts
     */
    float getVoltage();

    /**
     * Returns computed percentage of battery state
     * @return level of battery life
     */
    short getPercentage();

    /**
     * Compute estimated battery life. To use this function is needed to use update function which takes care of actualizing battery state history
     * @return estimated remaining time in minutes
     */
    int getRemainingTime();

    /**
     * This function takes care about actualizing battery values for estimating battery life.
     * It is best to run this function in longer intevales such as once a minute(or in even longer interval)
     */
    void update();
};


#endif
